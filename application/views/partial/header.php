<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>Sistem Informasi Pendidikan</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="Aegisdev" name="author" />
   <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/cloud-admin.css" >
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themes/nature.css" id="skin-switcher" >
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/responsive.css" >
   <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/animatecss/animate.min.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap-daterangepicker/daterangepicker-bs3.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/jquery-todo/css/styles.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar.min.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/gritter/css/jquery.gritter.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/jqgrid/css/ui.jqgrid.min.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatables/media/css/jquery.dataTables.min.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatables/media/assets/css/datatables.min.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatables/extras/TableTools/media/css/TableTools.min.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/panel/css/normalize.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/panel/css/demo.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/panel/css/component.css" />
   <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
</head>
<!-- END HEAD -->

