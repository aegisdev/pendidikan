<div id="sidebar" class="sidebar">
	<div class="sidebar-menu nav-collapse">
		<div class="divide-20"></div>
		<!-- SEARCH BAR -->
		<div id="search-bar">
			<input class="search" type="text" placeholder="Search"><i class="fa fa-search search-icon"></i>
		</div>
		<!-- /SEARCH BAR -->
		<ul>
			<li class="active sub-menu">
				<a href="<?=base_url();?>" class="parent-menu">
				<i class="fa fa-arrows-h fa-fw"></i> <span class="menu-text">Panel</span>
				<span class="selected"></span>
				</a>					
			</li>
			<li class="sub-menu">
				<a href="<?=base_url();?>#/home/dashboard" class="parent-menu">
				<i class="fa fa-dashboard fa-fw"></i> <span class="menu-text">Dashboard</span>
				<span class="selected"></span>
				</a>					
			</li>
			<li class="sub-menu">
				<a href="<?=base_url();?>#/home/kotaksurat" class="parent-menu">
				<i class="fa fa-envelope-o fa-fw"></i> <span class="menu-text">Kotak Surat</span>
				<span class="selected"></span>
				</a>					
			</li>
			<li class="sub-menu">
				<a href="<?=base_url();?>#/home/programpendidikan" class="parent-menu">
				<i class="fa fa-bookmark-o fa-fw"></i> <span class="menu-text">Program Pendidikan</span>
				<span class="selected"></span>
				</a>					
			</li>
			<li class="sub-menu">
				<a href="<?=base_url();?>" class="parent-menu">
				<i class="fa fa-th-large fa-fw"></i> <span class="menu-text">Sistem Pendidikan</span>
				<span class="selected"></span>
				</a>					
			</li>
			<li class="sub-menu">
				<a href="<?=base_url();?>" class="parent-menu">
				<i class="fa fa-file-text-o fa-fw"></i> <span class="menu-text">Laporan</span>
				<span class="selected"></span>
				</a>					
			</li>
			<li class="has-sub sub-menu">
				<a href="javascript:;" class="parent-menu">
				<i class="fa fa-folder-open fa-fw"></i> <span class="menu-text">Data Master</span>
				<span class="arrow"></span>
				</a>
				<ul class="sub">
					<li><a class="child-menu" href="#/home/kurikulum"><span class="sub-menu-text">Kurikulum</span></a></li>
					<li><a class="child-menu" href="#/home/gadik_organik"><span class="sub-menu-text">Gadik Organik</span></a></li>
					<li><a class="child-menu" href="#/home/gadik_non_organik"><span class="sub-menu-text">Gadik Non Organik</span></a></li>
					<li><a class="child-menu" href="#/home/gapendik"><span class="sub-menu-text">Gapendik</span></a></li>
					<li><a class="child-menu" href="#/home/metode_pengajaran"><span class="sub-menu-text">Metode Pengajaran</span></a></li>
					<li><a class="child-menu" href="#/home/alins_alongins"><span class="sub-menu-text">Alins / Alongins</span></a></li>
					<li><a class="child-menu" href="#/home/fasilitas_pendidikan"><span class="sub-menu-text">Fasilitas Pendidikan</span></a></li>
				</ul>
			</li>
			<li class="sub-menu">
				<a href="<?=base_url();?>#/home/pengguna" class="parent-menu">
				<i class="fa fa-user fa-fw"></i> <span class="menu-text">Manajemen Pengguna</span>
				<span class="selected"></span>
				</a>					
			</li>
			<li class="has-sub sub-menu">
				<a href="javascript:;" class="parent-menu">
				<i class="fa fa-warning fa-fw"></i> <span class="menu-text">Bantuan</span>
				<span class="arrow"></span>
				</a>
				<ul class="sub">
					<li><a class="child-menu" href="#/home/faq"><span class="sub-menu-text">Faq</span></a></li>
					<li><a class="child-menu" href="#"><span class="sub-menu-text">User Guide</span></a></li>
					<li><a class="child-menu" href="#"><span class="sub-menu-text">Ticket Support</span></a></li>
				</ul>
			</li>
		</ul>
		<!-- /SIDEBAR MENU -->
	</div>
</div>
