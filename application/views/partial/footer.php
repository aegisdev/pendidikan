	<!-- BEGIN JAVASCRIPTS -->
	
	<!-- JQUERY -->
	<script src="<?php echo base_url();?>assets/jquery/jquery-2.0.3.min.js"></script>
	
	<!-- JQUERY UI-->
	<script src="<?php echo base_url();?>assets/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	
	<!-- BOOTSTRAP -->
	<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
	
	<!-- DATA TABLES -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/datatables/media/assets/js/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/datatables/extras/TableTools/media/js/TableTools.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/datatables/extras/TableTools/media/js/ZeroClipboard.min.js"></script>
	
	<!-- DATE RANGE PICKER -->
	<script src="<?php echo base_url();?>assets/bootstrap-daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	
	<!-- BLOCK UI -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	
	<!-- SPARKLINES -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/sparklines/jquery.sparkline.min.js"></script>
	
	<!-- EASY PIE CHART -->
	<script src="<?php echo base_url();?>assets/jquery-easing/jquery.easing.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/easypiechart/jquery.easypiechart.min.js"></script>
	
	<!-- FLOT CHARTS -->
	<script src="<?php echo base_url();?>assets/flot/jquery.flot.min.js"></script>
	<script src="<?php echo base_url();?>assets/flot/jquery.flot.time.min.js"></script>
    <script src="<?php echo base_url();?>assets/flot/jquery.flot.selection.min.js"></script>
	<script src="<?php echo base_url();?>assets/flot/jquery.flot.resize.min.js"></script>
    <script src="<?php echo base_url();?>assets/flot/jquery.flot.pie.min.js"></script>
    <script src="<?php echo base_url();?>assets/flot/jquery.flot.stack.min.js"></script>
    <script src="<?php echo base_url();?>assets/flot/jquery.flot.crosshair.min.js"></script>
	
	<!-- TODO -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/jquery-todo/js/paddystodolist.js"></script>
	
	<!-- TIMEAGO -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/timeago/jquery.timeago.min.js"></script>
	
	<!-- FULL CALENDAR -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/fullcalendar/fullcalendar.min.js"></script>
	
	<!-- COOKIE -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/jQuery-Cookie/jquery.cookie.min.js"></script>
	
	<!-- GRITTER -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/gritter/js/jquery.gritter.min.js"></script>

	<!-- HIGHCHART -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/chart-master/highcharts.js"></script>
	
	<!-- BOOTBOX -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/bootbox/bootbox.min.js"></script>

	<!-- JQGRID -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/jqgrid/js/grid.locale-en.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/jqgrid/js/jquery.jqGrid.min.js"></script>

	<!-- FULL CALENDAR -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/fullcalendar/fullcalendar.min.js"></script>

	<!-- The basic File Upload plugin -->
	<script src="<?php echo base_url();?>assets/jquery-upload/js/jquery.fileupload.min.js"></script>
	<!-- The File Upload processing plugin -->
	<script src="<?php echo base_url();?>assets/jquery-upload/js/jquery.fileupload-process.min.js"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="<?php echo base_url();?>assets/jquery-upload/js/jquery.fileupload-image.min.js"></script>
	<!-- The File Upload audio preview plugin -->
	<script src="<?php echo base_url();?>assets/jquery-upload/js/jquery.fileupload-audio.min.js"></script>
	<!-- The File Upload video preview plugin -->
	<script src="<?php echo base_url();?>assets/jquery-upload/js/jquery.fileupload-video.min.js"></script>
	<!-- The File Upload validation plugin -->
	<script src="<?php echo base_url();?>assets/jquery-upload/js/jquery.fileupload-validate.min.js"></script>
	<!-- The File Upload user interface plugin -->
	<script src="<?php echo base_url();?>assets/jquery-upload/js/jquery.fileupload-ui.min.js"></script>
	<!-- The main application script -->
	<script src="<?php echo base_url();?>assets/jquery-upload/js/main.js"></script>
	
	<!-- CUSTOM SCRIPT -->
	<script src="<?php echo base_url();?>js/script.js"></script>
	<script src="<?php echo base_url();?>js/inbox.js"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("index");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
	
	<!-- router -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/director.min.js"></script>

	<!--routing script-->
	<script type="text/javascript">

	var image_load = "<div class='ajax_loading'><img src='<?php echo base_url();?>img/loaders/12.gif'/></div>";

	$(document).ready(function(){

		if(window.location.hash == "" || window.location.hash == "#/" || window.location.hash.length > 0){
			window.location = '<?php echo base_url();?>#/home/panel';
		}

		var routes = {
			  '/home/panel' : Panel,
			  '/home/dashboard' : mainDashboard,
			  '/home/kotaksurat' : kotakSurat,
			  '/home/programpendidikan' : programPendidikan,
			  '/home/faq' : faq,
			  '/home/pengguna' : pengguna,
			  '/home/kurikulum' : Kurikulum,
			  '/home/gadik_organik' : gadikOrganik,
			  '/home/gadik_non_organik' : gadikNonorganik,
			  '/home/gapendik' : gapendik,
			  '/home/metode_pengajaran' : metodePengajaran,
			  '/home/alins_alongins' : alinsAlongins,
			  '/home/fasilitas_pendidikan' : fasilitasPendidikan
		}
		
		var router = Router(routes);
		router.init();
	});
	  
	 $('.child-menu').click(function(ev){
        //remove all parent current active
        $('.sub-menu').removeClass('active');
        //add parent current active
        //$(this).parent().parent().parent().addClass('active');

        //remove all current active
        $('.child-menu').parent().removeClass('active');
        //add current active
        $(this).parent().addClass('active');

        modifyBreadcrumb(this, 1);
      });

      $('.parent-menu').click(function(){
        //remove all parent current active
        $('.sub-menu').removeClass('active');
        //remove all current active
        $('.child-menu').parent().removeClass('active');

        //add parent current active
        //$(this).parent().addClass('active');  

        modifyBreadcrumb($(this).children('span'), 2);
      });

      function modifyBreadcrumb(el,type){
        $('.page-title').show();
        $('.breadcrumb').show();
        $('.page-title').html('<h3 class="content-title pull-left">'+$(el).html()+'</h3>');

        var bc =  '<li>' + '<i class="fa fa-home"></i> <a href="<?=base_url();?>">Home</a>' + '</li>';
        if(type == 1){
          bc += '<li>' +''+ $(el).parent().parent().siblings('a').children('span').html() + '</li>';
          bc += '<li>' + $(el).html() + '</li>';
        } else {
          bc += '<li>' +  $(el).html() + '</li>';
        }
        $('.breadcrumb').html(bc);
      }

      function initView(idToShow, docTitle){
	    $('.page-content').hide();
	    $('.page-content').empty();
	    $('#' + idToShow).show();
	    document.title = docTitle;
	  }

	function load(url,page)
	{
		$.ajax({
		  url: '<?php echo base_url();?>home/'+url,
		  beforeSend: function(){ $('.page-content').html(image_load); }
		})
		.done(function(response, textStatus, jqhr){
		  initView(page,'Sistem Informasi Pendidikan');
		  $('#'+page).html(response);
		}) 
		.fail(function(){

		});
	} 

	function Panel()        		{ load('panel','panel'); }
	function mainDashboard()        { load('dashboard','main-dashboard'); }
	function kotakSurat()           { load('kotaksurat','kotaksurat'); }
	function programPendidikan()    { load('programpendidikan','programpendidikan'); }	  
	function faq()    				{ load('faq','faq'); }	  
	function pengguna()    			{ load('pengguna','pengguna'); }	

	function Kurikulum()        	{ load('kurikulum','kurikulum'); }
	function gadikOrganik()         { load('gadik_organik','gadik-organik'); }
	function gadikNonorganik()      { load('gadik_non_organik','gadik-non-organik'); }
	function gapendik()   		    { load('gapendik','gapendik'); }	  
	function metodePengajaran()     { load('metode_pengajaran','metode-pengajaran'); }	  
	function alinsAlongins()    	{ load('alins_alongins','alins-alongins'); }  
	function fasilitasPendidikan()	{ load('fasilitas_pendidikan','fasilitas-pendidikan'); }  
	</script>


   <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

   <!-- END JAVASCRIPTS -->   
</html>
