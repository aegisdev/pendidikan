<!DOCTYPE html>
<html>
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SISTEM INFORMASI AKADEMIK</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>css/simak-style.css" media="screen">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700,400' rel='stylesheet' type='text/css'>
    <script src="<?= base_url();?>js/jquery-1.3.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
(function($){
    //Resize image on ready or resize
    $.fn.supersize = function() {  
	//Invoke the resizenow() function on document ready
	$(document).ready(function() {
	    $('#supersize').resizenow();
	});
	//Invoke the resizenow() function on browser resize
	$(window).bind("resize", function() {
    $('#supersize').resizenow();
	});
    };
    //Adjust image size
    $.fn.resizenow = function() {
	//Define starting width and height values for the original image
	var startwidth = 1157;  
	var startheight = 723;
	//Define image ratio
	var ratio = startheight/startwidth;
	//Gather browser dimensions
	var browserwidth = $(window).width();
	var browserheight = $(window).height();
	//Resize image to proper ratio
	if ((browserheight/browserwidth) > ratio) {
	    $(this).height(browserheight);
	    $(this).width(browserheight / ratio);
	    $(this).children().height(browserheight);
	    $(this).children().width(browserheight / ratio);
	} else {
	    $(this).width(browserwidth);
	    $(this).height(browserwidth * ratio);
	    $(this).children().width(browserwidth);
	    $(this).children().height(browserwidth * ratio);
	}
	$(this).children().css('left', (browserwidth - $(this).width())/1);
	$(this).children().css('top', 0);//(browserheight - $(this).height())*0);
    };
})(jQuery);
	
$(document).ready(function() {
    $("div#supersize").supersize();
});
</script>
  </head>
  <body>
    <div id="wrapper">
    	<div id="simak">
        <div id="content">
        	
        	<div id="greeting">
            	<h1>SISTEM INFORMASI AKADEMIK</h1>
                <div class="boxes">
                    <div class="welcome">
                        <h3>Pendidikan yang dilandasi Professionalisme,<br>semangat juang dan soliditas TNI<br>bersama segenap komponen bangsa<br>siap menjaga kedaulatan dan keutuhan<br>wilayah NKRI</h3>
                    </div>
                
                    <div class="box-login">
                        <form id="simak-login" method="post" action="<?php base_url();?>do_login">
                            <input type="hidden" name="action" value="login">
                            <input type="hidden" name="hide" value="">
                            
                            <input type="text" name="username" placeholder="Username">
                            <input type="password" name="password" placeholder="Password">
                            <p class="alert">Username atau password tidak cocok</p>
                            <p class="lupa">Lupa password? <a href="#">klik disini</a></p>
                            <button class="btn_gold" type="submit" name="kirim">
                                <img src="<?= base_url();?>img/icon_unlock.png" alt=""> Login
                            </button>
                        </form>
                        <div class="wipe"></div>
                    </div>
                     <div class="wipe"></div>
                </div>
                <div class="wipe"></div>
                <p class="copyright-login">&copy; 2014</p>
                <div class="wipe"></div>
            </div>
        </div>
        <!--<div id="bg-content"> </div>-->
    </div>
    </div>
    <div id="supersize"> <img src="<?= base_url();?>img/bg-login_1157x723.jpg"> </div>
  </body>
</html>
