<style type="text/css">
	.box.border.green > .box-title{
		background-color: #8e9e82;
	}
</style>
<div class="box border green">
	<div class="box-title">
		<h4><i class="fa fa-bookmark-o"></i> <span class="hidden-inline-mobile">Alins/Alongins</span></h4>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				<button class="btn btn-success" id="tambah" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Alins/Alongins</button>
			</div>
			<div class="col-md-3">&nbsp;</div>
			<div class="col-md-3">
				<div class="input-group pull-right">
				  <input type="text" class="form-control" placeholder="Cari ...">
				  <span class="input-group-addon"><i class="fa fa-search"></i></span>
				</div>
			</div>
		</div>
		<div class="divide-20"></div>
		<div class="row">
			<div class="col-md-12">
				<table id="list-program" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="20px;" class="center hidden-xs">No.</th>
							<th></th>
							<th class="center hidden-xs">No. Iventaris</th>
							<th class="center hidden-xs">Jenis</th>
							<th class="center hidden-xs">Tipe</th>
							<th class="center hidden-xs">Nama Barang</th>
							<th class="center hidden-xs">Keterangan</th>
						</tr>						
					</thead>
					<tbody>
						<tr class="gradeX">
							<td class="center hidden-xs">1</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary edit-kelas"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger delete-kelas"><i class="fa fa-trash-o"></i></button>
							</td>
							<td></td>
							<td class="center hidden-xs"></td>
							<td class="center hidden-xs"> </td>
							<td class="center hidden-xs"> </td>
							<td class="center hidden-xs"> </td>							
						</tr>	
						<tr class="gradeX">
							<td class="center hidden-xs">2</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary edit-kelas"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger delete-kelas"><i class="fa fa-trash-o"></i></button>
							</td>
							<td></td>
							<td class="center hidden-xs"></td>
							<td class="center hidden-xs"> </td>
							<td class="center hidden-xs"> </td>
							<td class="center hidden-xs"></td>							
						</tr>	<tr class="gradeX">
							<td class="center hidden-xs">3</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary edit-kelas"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger delete-kelas"><i class="fa fa-trash-o"></i></button>
							</td>
							<td></td>
							<td class="center hidden-xs"></td>
							<td class="center hidden-xs"></td>
							<td class="center hidden-xs"></td>
							<td class="center hidden-xs"></td>							
						</tr>						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:999999">
	<div class="modal-dialog" style="width: 600px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Tambah Alins/Alongins</h4>
			</div>							
			<div class="modal-body">
				<form class="form-horizontal" role="form">
					
				  	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">No. Iventaris</label>
						<div class="col-sm-8">
					 	 <input type="email" class="form-control">
						</div>
				 	</div> 	
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Jenis</label>
						<div class="col-sm-8">
					 	 <input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Tipe</label>
						<div class="col-sm-8">
					 	 <input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">NRP</label>
						<div class="col-sm-8">
					 	 <input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Nama Barang</label>
						<div class="col-sm-8">
					 	 <input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Keterangan</label>
						<div class="col-sm-8">
					 	 <textarea type="email" class="form-control"></textarea>
						</div>
				 	</div>				 
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"> Batal</button>
				<button type="button" class="btn btn-success">Simpan</button>
		    </div>
		</div>
	</div>
</div>

<script>

	$('#list-kurikulum').dataTable({
		"sDom": "<'clear'>t<'pull-left'i><'pull-right'p>","oLanguage": {	"oPaginate": { "sPrevious": "Prev","sNext": "Next" } },"aoColumns": [null,null,null,null,null,null,null,{ "bSortable": false }	]
	});


</script>