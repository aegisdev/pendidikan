<style type="text/css">
	.box.border.green > .box-title{
		background-color: #8e9e82;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="box border green">
			<div class="box-title">
				<h4><i class="fa fa-envelope"></i>Kotak Surat</h4>
			</div>
			<div class="box-body">
				<div class="emailHeader row">
					<div class="emailTitle">
					  <div class="col-md-2">
						<!--<img class="img-responsive pull-left" alt="Cloud Admin Logo" src="img/logo/logo-alt.png">-->
						<a href="javascript:;" data-title="Compose" class="btn btn-success"> 
							Compose
						</a>
					  </div>
					  <div class="col-md-10">
						  <form class="form-inline hidden-xs" action="index.html">
							 <div class="input-group input-medium">
								<input type="text" class="form-control" placeholder="Search Inbox">
								<span class="input-group-btn">                   
								<button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>
								</span>
							 </div>
						  </form>
					  </div>
				   </div>
				</div>
				<!-- /TOP ROW -->
				<hr>
				<!-- INBOX -->
				<div class="row email">
						<div id="list-toggle" class="col-md-2">
						<!--
						<ul class="list-unstyled">
							<li class="composeBtn">
								 <a href="javascript:;" data-title="Compose" class="btn btn-danger"> 
									Compose
								 </a>
							 </li>
						</ul>
						-->
					   <ul class="emailNav nav nav-pills nav-stacked margin-bottom-10">									
						  <li class="inbox active" >
							 <a href="javascript:;" data-title="Inbox">
								<i class="fa fa-inbox fa-fw"></i> Inbox (2)
							 </a>
						  </li>
						  <li class="starred">
							<a href="javascript:;" data-title="Starred">
								<i class="fa fa-star fa-fw"></i> Starred
							</a>
						  </li>
						  <li class="sent">
							  <a href="javascript:;"  data-title="Sent">
								<i class="fa fa-mail-forward fa-fw"></i> Sent Items
							  </a>
						  </li>
						  <li class="draft">
							<a href="javascript:;" data-title="Draft">
								<i class="fa fa-files-o fa-fw"></i> Drafts
							</a>
						  </li>
						  <li class="spam">
							<a href="javascript:;" data-title="Spam">
								<i class="fa fa-ban fa-fw"></i> Spam
							</a>
						  </li>
						  <li class="trash">
							<a href="javascript:;" data-title="Trash">
								<i class="fa fa-trash-o fa-fw"></i> Trash
							</a>
						  </li>
					   </ul>
					</div>
					<div class="col-md-10">
					   <div class="emailContent"><html>
   <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8">
   </head>
   <body>
      <table class="table table-hover">
         <thead>
            <tr>
               <th colspan="4">
                  <input type="checkbox">
                     <a class="btn btn-light-grey" href="#"><i class="fa fa-refresh fa-lg"></i></a>
					 <div class="btn-group">
						 <button class="btn btn-light-grey dropdown-toggle" data-toggle="dropdown"> More
							<i class="fa fa-caret-down"></i>
						 </button>
						 <ul class="dropdown-menu context" role="menu">
							<li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
							<li><a href="#"><i class="fa fa-ban"></i> Report Spam</a></li>
							<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
						 </ul>
					 </div>
               </th>
               <th colspan="3">
					<div class="btn-group pull-right">
						 <button class="btn btn-light-grey dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-cog fa-lg"></i> <i class="fa fa-caret-down"></i>
						 </button>
						 <ul class="dropdown-menu context" role="menu">
							<li><a href="#"><i class="fa fa-cogs"></i> Settings</a></li>
							<li><a href="#"><i class="fa fa-desktop"></i> Configure Inbox</a></li>
							<li><a href="#"><i class="fa fa-exclamation"></i> Help</a></li>
						 </ul>
					 </div>
			   </th>
            </tr>
         </thead>
         <tbody>
            <tr class="new">
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail  width-10"></td>
               <td class="viewEmail  hidden-xs">Fametaxi Inc.</td>
               <td class="viewEmail "><span class="label label-success">New</span> Design document approved</td>
               <td class="viewEmail  text-right">08:20 PM</td>
            </tr>
            <tr class="new">
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail  width-10"></td>
               <td class="viewEmail hidden-xs">Steve Jobs</td>
               <td class="viewEmail"><span class="label label-success">New</span> Please buy our new iPhone</td>
               <td class="viewEmail text-right">Oct 18</td>
            </tr>
            <tr class="new">
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star starred"></i></td>
			   <td class="viewEmail width-10"><i class="fa fa-paperclip"></i></td>
               <td class="viewEmail hidden-xs">VMWare Billdesk</td>
               <td class="viewEmail">Billing information for the month of August</td>
               <td class="viewEmail text-right">Oct 03</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail width-10"><i class="fa fa-camera"></i></td>
               <td class="viewEmail hidden-xs">Facebook</td>
               <td class="viewEmail">John Doe, Liz have upcoming birthdays</td>              
               <td class="viewEmail text-right">Sep 14</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
               <td class="viewEmail width-10"></td>
               <td class="viewEmail hidden-xs">LinkedIn</td>
               <td class="viewEmail"><span class="label label-danger">Respond</span> Consetetur sadipscing elitry</td>
               <td class="viewEmail text-right">Sep 15</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star starred"></i></td>
			   <td class="viewEmail width-10"><i class="fa fa-paperclip"></i></td>
               <td class="viewEmail hidden-xs">Jane Doe</td>
               <td class="viewEmail">Dolor sit amet, consectetuer adipiscing</td>               
               <td class="viewEmail text-right">Aug 14</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star starred"></i></td>
			   <td class="viewEmail width-10"></td>
               <td class="viewEmail hidden-xs">John Doe</td>
               <td class="viewEmail"><span class="label label-warning">Read Later</span> Consetetur sadipscing elitry</td>               
               <td class="viewEmail text-right">Aug 15</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail width-10"></td>
               <td class="viewEmail hidden-xs">LinkedIn</td>
               <td class="viewEmail viewEmail">Sed diam nonumy eirmod tempor invidu</td>
               <td class="viewEmail text-right">Aug 14</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail width-10"></td>
               <td class="viewEmail hidden-xs">Jane Doe</td>
               <td class="viewEmail viewEmail">Consetetur sadipscing elitry</td>
               <td class="viewEmail text-right">July 15</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail width-10"></td>
               <td class="viewEmail hidden-xs">Facebook</td>
               <td class="viewEmail viewEmail"><span class="label label-warning">Read Later</span> Sed diam nonumy eirmod tempor invidu</td>
               <td class="viewEmail text-right">July 14</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star starred"></i></td>
			   <td class="viewEmail width-10"><i class="fa fa-camera"></i></td>
               <td class="viewEmail hidden-xs">John Doe</td>
               <td class="viewEmail">Consetetur sadipscing elitry</td>               
               <td class="viewEmail text-right">June 15</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star starred"></i></td>
			   <td class="viewEmail width-10"><i class="fa fa-paperclip"></i></td>
               <td class="hidden-xs">LinkedIn</td>
               <td class="viewEmail">Sed diam nonumy eirmod tempor invidu</td>
               <td class="viewEmail text-right">June 14</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star starred"></i></td>
			   <td class="viewEmail width-10"><i class="fa fa-paperclip"></i></td>
               <td class="viewEmail hidden-xs">Twitter</td>
               <td class="viewEmail">Consetetur sadipscing elitry</td>
               <td class="viewEmail text-right">April 15</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail width-10"></td>
               <td class="hidden-xs">Facebook</td>
               <td class="viewEmail viewEmail"><span class="label label-info">To Do</span> Sed diam nonumy eirmod tempor invidu</td>               
               <td class="viewEmail text-right">April 14</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail width-10"></td>
               <td class="viewEmail hidden-xs">Max Doe</td>
               <td class="viewEmail"><span class="label label-info">To Do</span> Consetetur sadipscing elitry</td>
               <td class="viewEmail text-right">April 15</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail width-10"></td>
               <td class="viewEmail hidden-xs">Dribbble</td>
               <td class="viewEmail">Sed diam nonumy eirmod tempor invidu</td>
               <td class="viewEmail text-right">April 14</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star starred"></i></td>
			   <td class="viewEmail width-10"><i class="fa fa-paperclip"></i></td>
               <td class="viewEmail hidden-xs">Barack Obama</td>
               <td class="viewEmail">Consetetur sadipscing elitry</td>               
               <td class="viewEmail text-right">March 15</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star starred"></i></td>
			   <td class="viewEmail width-10"><i class="fa fa-paperclip"></i></td>
               <td class="viewEmail hidden-xs">Facebook</td>
               <td class="viewEmail viewEmail"><span class="label label-info">To Do</span> Sed diam nonumy eirmod tempor invidu</td>
               <td class="viewEmail text-right">March 14</td>
            </tr>
            <tr>
               <td class="width-10">
                  <input  type="checkbox">
               </td>
               <td class="width-10"><i class="fa fa-star"></i></td>
			   <td class="viewEmail width-10"></td>
               <td class="viewEmail hidden-xs">John Doe</td>
               <td class="viewEmail">Consetetur sadipscing elitry</td>              
               <td class="viewEmail text-right">March 15</td>
            </tr>
         </tbody>
		 <thead>
            <tr>
               <th colspan="4">
               </th>
               <th class="emailPager" colspan="3">
                  <span class="emailPagerCount">1-30 of 1343</span>
                  <a class="btn btn-sm btn-light-grey"><i class="fa fa-angle-left"></i></a>
                  <a class="btn btn-sm btn-light-grey"><i class="fa fa-angle-right"></i></a>
               </th>
            </tr>
         </thead>
      </table>
   </body>
</html></div>
					</div>
				
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
