<body>
	<!-- HEADER -->
	<header class="navbar clearfix navbar-fixed-top" id="header">
		<div class="container">
			<div class="navbar-brand">
				<!-- COMPANY LOGO -->
				<a href="">
					<img src="img/logo.png" alt="Cloud Admin Logo" style="width:30px;height:30px;">
					<span style="color:#fff;left:95px;position:absolute;">Sistem Informasi Pendidikan</span>
				</a>
				<!-- /COMPANY LOGO -->
				<!-- SIDEBAR COLLAPSE -->
				<div id="sidebar-collapse" class="sidebar-collapse btn">
					<i class="fa fa-bars" 
						data-icon1="fa fa-bars" 
						data-icon2="fa fa-bars" ></i>
				</div>
				<!-- /SIDEBAR COLLAPSE -->
			</div>
			
			<!-- BEGIN TOP NAVIGATION MENU -->					
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->	
				<!--
				<li class="dropdown" id="header-notification">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-bell"></i>
						<span class="badge">1</span>						
					</a>
					<ul class="dropdown-menu notification">
						<li class="dropdown-title">
							<span><i class="fa fa-bell"></i> 1 Notifications</span>
						</li>
						<li>
							<a href="#">
								<span class="label label-primary"><i class="fa fa-comment"></i></span>
								<span class="body">
									<span class="message">Martin commented.</span>
									<span class="time">
										<i class="fa fa-clock-o"></i>
										<span>19 mins</span>
									</span>
								</span>
							</a>
						</li>
						<li class="footer">
							<a href="#">See all notifications <i class="fa fa-arrow-circle-right"></i></a>
						</li>
					</ul>
				</li>
				-->
				<!-- END NOTIFICATION DROPDOWN -->
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown user pull-right" id="header-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img alt="" src="img/avatars/avatar2.jpg" />
						<span class="username">Admin</span>
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#"><i class="fa fa-user"></i> My Profile</a></li>
						<li><a href="#"><i class="fa fa-cog"></i> Account Settings</a></li>
						<li><a href="<?php echo site_url();?>auth/logout"><i class="fa fa-power-off"></i> Log Out</a></li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
			</ul>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		
	</header>
	<!--/HEADER -->
	
	<!-- PAGE -->
	<section id="page">
		<!-- SIDEBAR -->
			<?php $this->load->view('partial/sidebar'); ?>
		<!-- /SIDEBAR -->
		
		<div id="main-content">
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						<div class="row">
							<div class="col-sm-12">
								<div class="page-header">
									<!-- STYLER -->
									
									<!-- /STYLER -->
									<!-- BREADCRUMBS -->
									<ul class="breadcrumb">
										<li>
											<i class="fa fa-home"></i>
											<a href="<?=base_url();?>">Home</a>
										</li>
										<li>Dashboard</li>
									</ul>
									<!-- /BREADCRUMBS -->
									<div class="clearfix">
										<h3 class="page-title content-title pull-left">Dashboard</h3>
									</div>
									<div class="description">Overview, Statistics and more</div>
								</div>
							</div>
						</div>
						<!-- /PAGE HEADER -->
						
						<!-- CONTENT -->
						<div class="page-content" id="panel"></div>
						<div class="page-content" id="main-dashboard"></div>
						<div class="page-content" id="kotaksurat"></div>
						<div class="page-content" id="programpendidikan"></div>
						<div class="page-content" id="faq"></div>
						<div class="page-content" id="pengguna"></div>
						<div class="page-content" id="kurikulum"></div>
						<div class="page-content" id="gadik-organik"></div>
						<div class="page-content" id="gadik-non-organik"></div>
						<div class="page-content" id="gapendik"></div>
						<div class="page-content" id="metode-pengajaran"></div>
						<div class="page-content" id="alins-alongins"></div>
						<div class="page-content" id="fasilitas-pendidikan"></div>
						<!-- /CONTENT -->
						
						<div class="row footer" style="opacity:0.7;background: none repeat scroll 0 0 #FFF;border-top:1px solid #eee;bottom: 0;color: #666666;font-size: 12px;padding: 10px;position: fixed;width: 100%;z-index: 10;">
						   Cloud Admin Template &copy; <?=date('Y');?>
					   </div>
						<!--
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/PAGE -->