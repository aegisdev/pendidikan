<style type="text/css">
	.box.border.green > .box-title{
		background-color: #8e9e82;
	}
</style>
<div class="box border green">
	<div class="box-title">
		<h4><i class="fa fa-bookmark-o"></i> <span class="hidden-inline-mobile">Gadik Organik</span></h4>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				<button class="btn btn-success" id="tambah" data-toggle="modal" data-target="#myModal">
					<i class="fa fa-plus"></i> Tambah Gadik Organik
				</button>
			</div>
			<div class="col-md-3">&nbsp;</div>
			<div class="col-md-3">
				<div class="input-group pull-right">
				  <input type="text" class="form-control" placeholder="Cari ...">
				  <span class="input-group-addon"><i class="fa fa-search"></i></span>
				</div>
			</div>
		</div>
		<div class="divide-20"></div>
		<div class="row">
			<div class="col-md-12">
				<table id="list-gadik-organik" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="20px;" class="center hidden-xs">No.</th>
							<th>&nbsp;</th>
							<th class="center hidden-xs">Nama</th>
							<th class="center hidden-xs">Pangkat</th>
							<th class="center hidden-xs">Korps</th>
							<th class="center hidden-xs">NRP</th>
							<th class="center hidden-xs">Jabatan</th>
							<th class="center hidden-xs">Keterangan</th>
						</tr>
					</thead>
					<tbody>
						<tr class="gradeX">
							<td class="center hidden-xs">1</td>
							<td class="center hidden-xs" nowrap>
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">2</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">3</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">4</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">5</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">6</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">7</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:999999">
	<div class="modal-dialog" style="width: 600px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Tambah Gadik Organik</h4>
			</div>							
			<div class="modal-body">
				<form class="form-horizontal" role="form">
				  	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Nama</label>
						<div class="col-sm-8">
					 		<input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Pangkat</label>
						<div class="col-sm-8">
					 		<input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Pangkat / Korps</label>
						<div class="col-sm-8">
					 		<input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">NRP</label>
						<div class="col-sm-8">
					 		<input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Jabatan</label>
						<div class="col-sm-8">
					 	 <input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Keterangan</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success pull-left"><i class="fa fa-check"></i> Simpan</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>
		</div>
	</div>
</div>

<script>

	$('#list-gadik-organik').dataTable({
		"sDom": "<'clear'>t<'pull-left'i><'pull-right'p>","oLanguage": {	"oPaginate": { "sPrevious": "Prev","sNext": "Next" } },"aoColumns": [null,{ "bSortable": false },null,null,null,null,null,null	]
	});


</script>