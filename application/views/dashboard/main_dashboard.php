<style type="text/css">
	.box.border.green > .box-title{
		background-color: #8e9e82;
	}

</style>
<div class="row">
	<div class="col-md-12">
		<!-- BOX -->
		<div class="box border green">
			<div class="box-title">
				<h4><i class="fa fa-table"></i>Jadwal Pelajaran</h4>
			</div>
			<div class="box-body">
				<div class="tabbable header-tabs">
				  <ul class="nav nav-tabs">
				  	<li>
				  		<a href="#minggu" data-toggle="tab"><i class="fa fa-circle-o"></i><span class="hidden-inline-mobile">Minggu</span></a>
				  	</li>
				  	<li>
				  		<a href="#sabtu" data-toggle="tab"><i class="fa fa-circle-o"></i><span class="hidden-inline-mobile">Sabtu</span></a>
				  	</li>
				  	<li>
				  		<a href="#jumat" data-toggle="tab"><i class="fa fa-circle-o"></i><span class="hidden-inline-mobile">Jum'at</span></a>
				  	</li>
				  	<li>
				  		<a href="#kamis" data-toggle="tab"><i class="fa fa-circle-o"></i><span class="hidden-inline-mobile">Kamis</span></a>
				  	</li>
				  	<li>
				  		<a href="#rabu" data-toggle="tab"><i class="fa fa-circle-o"></i><span class="hidden-inline-mobile">Rabu</span></a>
				  	</li>
				  	<li>
				  		<a href="#selasa" data-toggle="tab"><i class="fa fa-circle-o"></i><span class="hidden-inline-mobile">Selasa</span></a>
				  	</li>
				  	<li class="active">
				  		<a href="#senin" data-toggle="tab"><i class="fa fa-circle-o"></i><span class="hidden-inline-mobile">Senin</span></a>
				  	</li>
				  </ul>
				  <div class="tab-content">
				  	<div class="tab-pane fade in active" id="senin">
				  		<table class="table table-bordered">
								<thead>
								  <tr>
									<th width="16%">Jam / Ruangan</th>
									<th width="12%">RK - 100</th>
									<th width="12%">RK - 200</th>
									<th width="12%">RK - 300</th>
									<th width="12%">RK - 400</th>
									<th width="12%">RK - 500</th>
									<th width="12%">RK - 600</th>
									<th width="12%">RK - 700</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
									<td>07.00-08.00</td>
									<td>
										<div class="ms-sel-matpel ">olahraga</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>08.00-09.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">matematika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>09.00-10.00</td>
									<td>
										<div class="ms-sel-matpel ">Komputer</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">kimia</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>10.00-11.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Robotika</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Fisika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>11.00-12.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Psikologi</div><br />
									</td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>12.00-13.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Jurnalistik</div><br />
									</td>
									<td></td>
								  </tr>
								  <tr>
									<td>14.00-15.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Agama</div><br />
									</td>
								  </tr>
								</tbody>
							 </table>
				  	</div>
				  	<div class="tab-pane fade" id="selasa">
				  		<table class="table table-bordered">
								<thead>
								  <tr>
									<th width="16%">Jam / Ruangan</th>
									<th width="12%">RK - 100</th>
									<th width="12%">RK - 200</th>
									<th width="12%">RK - 300</th>
									<th width="12%">RK - 400</th>
									<th width="12%">RK - 500</th>
									<th width="12%">RK - 600</th>
									<th width="12%">RK - 700</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
									<td>07.00-08.00</td>
									<td>
										<div class="ms-sel-matpel ">olahraga</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>08.00-09.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">matematika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>09.00-10.00</td>
									<td>
										<div class="ms-sel-matpel ">Komputer</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">kimia</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>10.00-11.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Fisika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>11.00-12.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Psikologi</div><br />
									</td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>12.00-13.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Robotika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Jurnalistik</div><br />
									</td>
									<td></td>
								  </tr>
								  <tr>
									<td>14.00-15.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Agama</div><br />
									</td>
								  </tr>
								</tbody>
							 </table>
				  	</div>
				  	<div class="tab-pane fade" id="rabu">
				  		<table class="table table-bordered">
								<thead>
								  <tr>
									<th width="16%">Jam / Ruangan</th>
									<th width="12%">RK - 100</th>
									<th width="12%">RK - 200</th>
									<th width="12%">RK - 300</th>
									<th width="12%">RK - 400</th>
									<th width="12%">RK - 500</th>
									<th width="12%">RK - 600</th>
									<th width="12%">RK - 700</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
									<td>07.00-08.00</td>
									<td>
										<div class="ms-sel-matpel ">olahraga</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>08.00-09.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">matematika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>09.00-10.00</td>
									<td>
										<div class="ms-sel-matpel ">Komputer</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">kimia</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>10.00-11.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Robotika</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Fisika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>11.00-12.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Psikologi</div><br />
									</td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>12.00-13.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Jurnalistik</div><br />
									</td>
									<td></td>
								  </tr>
								  <tr>
									<td>14.00-15.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Agama</div><br />
									</td>
								  </tr>
								</tbody>
							 </table>
				  	</div>
				  	<div class="tab-pane fade" id="kamis">
				  		<table class="table table-bordered">
								<thead>
								  <tr>
									<th width="16%">Jam / Ruangan</th>
									<th width="12%">RK - 100</th>
									<th width="12%">RK - 200</th>
									<th width="12%">RK - 300</th>
									<th width="12%">RK - 400</th>
									<th width="12%">RK - 500</th>
									<th width="12%">RK - 600</th>
									<th width="12%">RK - 700</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
									<td>07.00-08.00</td>
									<td>
										<div class="ms-sel-matpel ">olahraga</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>08.00-09.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">matematika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>09.00-10.00</td>
									<td>
										<div class="ms-sel-matpel ">Komputer</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">kimia</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>10.00-11.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Fisika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>11.00-12.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Psikologi</div><br />
									</td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>12.00-13.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Robotika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Jurnalistik</div><br />
									</td>
									<td></td>
								  </tr>
								  <tr>
									<td>14.00-15.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Agama</div><br />
									</td>
								  </tr>
								</tbody>
							 </table>
				  	</div>
				  	<div class="tab-pane fade" id="jumat">
				  		<table class="table table-bordered">
								<thead>
								  <tr>
									<th width="16%">Jam / Ruangan</th>
									<th width="12%">RK - 100</th>
									<th width="12%">RK - 200</th>
									<th width="12%">RK - 300</th>
									<th width="12%">RK - 400</th>
									<th width="12%">RK - 500</th>
									<th width="12%">RK - 600</th>
									<th width="12%">RK - 700</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
									<td>07.00-08.00</td>
									<td>
										<div class="ms-sel-matpel ">olahraga</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>08.00-09.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">matematika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>09.00-10.00</td>
									<td>
										<div class="ms-sel-matpel ">Komputer</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">kimia</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>10.00-11.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Robotika</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Fisika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>11.00-12.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Psikologi</div><br />
									</td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>12.00-13.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Jurnalistik</div><br />
									</td>
									<td></td>
								  </tr>
								  <tr>
									<td>14.00-15.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Agama</div><br />
									</td>
								  </tr>
								</tbody>
							 </table>
				  	</div>
				  	<div class="tab-pane fade" id="sabtu">
				  		<table class="table table-bordered">
								<thead>
								  <tr>
									<th width="16%">Jam / Ruangan</th>
									<th width="12%">RK - 100</th>
									<th width="12%">RK - 200</th>
									<th width="12%">RK - 300</th>
									<th width="12%">RK - 400</th>
									<th width="12%">RK - 500</th>
									<th width="12%">RK - 600</th>
									<th width="12%">RK - 700</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
									<td>07.00-08.00</td>
									<td>
										<div class="ms-sel-matpel ">olahraga</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>08.00-09.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">matematika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>09.00-10.00</td>
									<td>
										<div class="ms-sel-matpel ">Komputer</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">kimia</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>10.00-11.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Fisika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>11.00-12.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Psikologi</div><br />
									</td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>12.00-13.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Robotika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Jurnalistik</div><br />
									</td>
									<td></td>
								  </tr>
								  <tr>
									<td>14.00-15.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Agama</div><br />
									</td>
								  </tr>
								</tbody>
							 </table>
				  	</div>
				  	<div class="tab-pane fade" id="minggu">
				  		<table class="table table-bordered">
								<thead>
								  <tr>
									<th width="16%">Jam / Ruangan</th>
									<th width="12%">RK - 100</th>
									<th width="12%">RK - 200</th>
									<th width="12%">RK - 300</th>
									<th width="12%">RK - 400</th>
									<th width="12%">RK - 500</th>
									<th width="12%">RK - 600</th>
									<th width="12%">RK - 700</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
									<td>07.00-08.00</td>
									<td>
										<div class="ms-sel-matpel ">olahraga</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>08.00-09.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">matematika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>09.00-10.00</td>
									<td>
										<div class="ms-sel-matpel ">Komputer</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">kimia</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>10.00-11.00</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Robotika</div><br />
									</td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Fisika</div><br />
									</td>
									<td></td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>11.00-12.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Psikologi</div><br />
									</td>
									<td></td>
									<td></td>
								  </tr>
								  <tr>
									<td>12.00-13.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Jurnalistik</div><br />
									</td>
									<td></td>
								  </tr>
								  <tr>
									<td>14.00-15.00</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<div class="ms-sel-matpel ">Agama</div><br />
									</td>
								  </tr>
								</tbody>
							 </table>
				  	</div>
				  </div>
			   </div>
			</div>
		</div>
		<!-- /BOX -->
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="box border green">
			<div class="box-title">
				<h4><i class="fa fa-calendar"></i>Tabel Data Kelas</h4>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<thead>
						<th width="20px">No.</th>
						<th>Kelas</th>
						<th>Jumlah Siswa</th>
					</thead>
					<tbody>
						<tr>
							<td>1.</td>
							<td>Kelas A</td>
							<td>100</td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Kelas B</td>
							<td>120</td>
						</tr>
						<tr>
							<td>3.</td>
							<td>Kelas C</td>
							<td>80</td>
						</tr>
						<tr>
							<td>4.</td>
							<td>Kelas D</td>
							<td>90</td>
						</tr>
						<tr>
							<td>5.</td>
							<td>Kelas E</td>
							<td>40</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="box border green">
			<div class="box-title">
				<h4><i class="fa fa-calendar"></i>Chart Data Kelas</h4>
			</div>
			<div class="box-body">
				<div id="chart-data-kelas"></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<!-- BOX -->
		<div class="box border green">
			<div class="box-title">
				<h4><i class="fa fa-calendar"></i>Kalender Akademik</h4>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div id='calendar-pimpinan'></div>
					</div>
				</div>
			</div>
		</div>
		<!-- /BOX -->
	</div>
	<div class="col-md-6">
		<!-- BOX -->
			<div class="box border green chat-window">
				<div class="box-title">
					<h4><i class="fa fa-comments"></i>Chat Window</h4>
				</div>
				<div class="box-body big">
					<div class="scroller" data-height="402px" data-always-visible="1" data-rail-visible="1" style="max-height:520px;overflow:auto;">
						<ul class="media-list chat-list">
							<li class="media">
							  <a class="pull-left" href="#">
								<img class="media-object" alt="Generic placeholder image" src="img/chat/headshot1.jpg">
							  </a>
							  <div class="media-body chat-pop">
								<h4 class="media-heading">Kapt. Andre <span class="pull-right"><i class="fa fa-clock-o"></i> <abbr class="timeago" title="Oct 9, 2013" >Oct 9, 2013</abbr> </span></h4>
								<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
								
							  </div>
							</li>
							<li class="media">
							  <a class="pull-right" href="#">
								<img class="media-object"  alt="Generic placeholder image" src="img/chat/headshot3.jpg">
							  </a>
							  <div class="pull-right media-body chat-pop mod">
								<h4 class="media-heading">Kapt. Bagas <span class="pull-left"><abbr class="timeago" title="Oct 10, 2013" >Oct 10, 2013</abbr> <i class="fa fa-clock-o"></i></span></h4></h4>
								Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
							  </div>
							</li>
							<li class="media">
							  <a class="pull-left" href="#">
								<img class="media-object"  alt="Generic placeholder image" src="img/chat/headshot1.jpg">
							  </a>
							  <div class="media-body chat-pop">
								<h4 class="media-heading">Kapt. Andre <span class="pull-right"><i class="fa fa-clock-o"></i> <abbr class="timeago" title="Oct 10, 2013" >Oct 10, 2013</abbr> </span></h4></h4>
								<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
							  </div>
							</li>
							<li class="media">
							  <a class="pull-right" href="#">
								<img class="media-object"  alt="Generic placeholder image" src="img/chat/headshot3.jpg">
							  </a>
							  <div class="pull-right media-body chat-pop mod">
								<h4 class="media-heading">Kapt. Bagas <span class="pull-left"><abbr class="timeago" title="Oct 11, 2013" >Oct 11, 2013</abbr> <i class="fa fa-clock-o"></i></span></h4></h4>
								Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
							  </div>
							</li>
							<li class="media">
							  <a class="pull-left" href="#">
								<img class="media-object"  alt="Generic placeholder image" src="img/chat/headshot1.jpg">
							  </a>
							  <div class="media-body chat-pop">
								<h4 class="media-heading">Kapt. Andre <span class="pull-right"><i class="fa fa-clock-o"></i> <abbr class="timeago" title="Oct 12, 2013" >Oct 12, 2013</abbr> </span></h4></h4>
								<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
							  </div>
							</li>
							<li class="media">
							  <a class="pull-right" href="#">
								<img class="media-object"  alt="Generic placeholder image" src="img/chat/headshot3.jpg">
							  </a>
							  <div class="pull-right media-body chat-pop mod">
								<h4 class="media-heading">Kapt. Bagas <span class="pull-left"><abbr class="timeago" title="Oct 12, 2013" >Oct 12, 2013</abbr> <i class="fa fa-clock-o"></i></span></h4></h4>
								Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
							  </div>
							</li>
						</ul>
					</div>
					<div class="divide-20"></div>
					<div class="chat-form">
						<div class="input-group"> 
							<input type="text" class="form-control" placeholder="Type something...really, it works"> 
							<span class="input-group-btn"> <button class="btn btn-primary" type="button"><i class="fa fa-check"></i></button> </span> 
						</div>
					</div>
				</div>
			</div>
			<!-- /BOX -->
		</div>
	</div>
</div>
<script type="text/javascript">

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

    $('#calendar-pimpinan').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        editable: true,
        droppable: false,
        events: [
            {
                title: 'Matematika',
								start: new Date(y, m, d, 12, 30),
								end: new Date(y, m, d, 14, 0),
								allDay: false,
								backgroundColor: Theme.colors.green,
            },
            {
                title: 'Fisika',
								start: new Date(y, m, d+3, 8, 0),
								end: new Date(y, m, d+3, 10, 0),
								allDay: false,
								backgroundColor: Theme.colors.primary,
            },
            {
                title: 'Kimia',
								start: new Date(y, m, d+5, 15, 0),
								end: new Date(y, m, d+5, 18, 0),
								allDay: false,
								backgroundColor: Theme.colors.grey,
            },
            {
                title: 'UTS Matematika',
								start: new Date(y, m, d+15, 15, 0),
								end: new Date(y, m, d+15, 18, 0),
								allDay: false,
								backgroundColor: Theme.colors.red,
            },
        ]
    });

		// Chart
		$('#chart-data-kelas').highcharts({
      chart: {
	          defaultSeriesType: 'column',
	          marginRight: 130,
	          marginBottom: 70
	      },
	      title: {
	          text: ''
	      },
	      xAxis: {
	          categories: ["Kelas A", "Kelas B", "Kelas C", "Kelas D", "Kelas E"]
	      },
	      yAxis: {
	          title: {
	              text: 'Jumlah Siswa'
	          }
	      },      
	      legend: {layout: 'vertical',align: 'right',verticalAlign: 'top',x: -10,y: 100,borderWidth: 0},
	      credits: { enabled: false },
	      series: [{
	          name: 'Jumlah Siswa',
	          data: [100, 120, 80, 90, 40]
	     }]
	  });
</script>