<style type="text/css">
	.box.border.green > .box-title{
		background-color: #8e9e82;
	}
</style>
<div class="box border green">
	<div class="box-title">
		<h4><i class="fa fa-bookmark-o"></i> <span class="hidden-inline-mobile">Pengelolaan Program</span></h4>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				<button class="btn btn-success" id="tambah" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah</button>
			</div>
			<div class="col-md-3">&nbsp;</div>
			<div class="col-md-3">
				<div class="input-group pull-right">
				  <input type="text" class="form-control" placeholder="Cari ...">
				  <span class="input-group-addon"><i class="fa fa-search"></i></span>
				</div>
			</div>
		</div>
		<div class="divide-20"></div>
		<div class="row">
			<div class="col-md-12">
				<table id="list-program" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="20px;" class="center hidden-xs">No.</th>
							<th>Program</th>
							<th class="center hidden-xs">Jml. Peserta</th>
							<th class="center hidden-xs">Durasi</th>
							<th class="center hidden-xs">Waktu Pelaksanaan</th>
							<th class="center hidden-xs">Tempat Pelaksanaan</th>
							<th class="center hidden-xs">Dokumen</th>
							<th class="center hidden-xs">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<tr class="gradeX">
							<td class="center hidden-xs">1</td>
							<td>SUSDANYON</td>
							<td class="center hidden-xs">20 Orang</td>
							<td class="center hidden-xs">90 Hari</td>
							<td class="center hidden-xs">Lihat Kalender</td>
							<td class="center hidden-xs">Pusdik Armed</td>
							<td class="center hidden-xs">file</td>
							<td class="center hidden-xs">
								<button class="btn btn-default edit-kelas"><i class="fa fa-pencil-square-o"></i></button>
								<button class="btn btn-default delete-kelas"><i class="fa fa-times"></i></button>
							</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">2</td>
							<td>SUSDANRAI</td>
							<td class="center hidden-xs">20 Orang</td>
							<td class="center hidden-xs">90 Hari</td>
							<td class="center hidden-xs">Lihat Kalender</td>
							<td class="center hidden-xs">Pusdik Armed</td>
							<td class="center hidden-xs">file</td>
							<td class="center hidden-xs">
								<button class="btn btn-default edit-kelas"><i class="fa fa-pencil-square-o"></i></button>
								<button class="btn btn-default delete-kelas"><i class="fa fa-times"></i></button>
							</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">3</td>
							<td>SUSPATIH</td>
							<td class="center hidden-xs">20 Orang</td>
							<td class="center hidden-xs">90 Hari</td>
							<td class="center hidden-xs">Lihat Kalender</td>
							<td class="center hidden-xs">Pusdik Armed</td>
							<td class="center hidden-xs">file</td>
							<td class="center hidden-xs">
								<button class="btn btn-default edit-kelas"><i class="fa fa-pencil-square-o"></i></button>
								<button class="btn btn-default delete-kelas"><i class="fa fa-times"></i></button>
							</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">4</td>
							<td>SUSPASTAFYON</td>
							<td class="center hidden-xs">20 Orang</td>
							<td class="center hidden-xs">90 Hari</td>
							<td class="center hidden-xs">Lihat Kalender</td>
							<td class="center hidden-xs">Pusdik Armed</td>
							<td class="center hidden-xs">file</td>
							<td class="center hidden-xs">
								<button class="btn btn-default edit-kelas"><i class="fa fa-pencil-square-o"></i></button>
								<button class="btn btn-default delete-kelas"><i class="fa fa-times"></i></button>
							</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">5</td>
							<td>SUSPATURBAK</td>
							<td class="center hidden-xs">20 Orang</td>
							<td class="center hidden-xs">90 Hari</td>
							<td class="center hidden-xs">Lihat Kalender</td>
							<td class="center hidden-xs">Pusdik Armed</td>
							<td class="center hidden-xs">file</td>
							<td class="center hidden-xs">
								<button class="btn btn-default edit-kelas"><i class="fa fa-pencil-square-o"></i></button>
								<button class="btn btn-default delete-kelas"><i class="fa fa-times"></i></button>
							</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">6</td>
							<td>SUSBAKURMED</td>
							<td class="center hidden-xs">20 Orang</td>
							<td class="center hidden-xs">90 Hari</td>
							<td class="center hidden-xs">Lihat Kalender</td>
							<td class="center hidden-xs">Pusdik Armed</td>
							<td class="center hidden-xs">file</td>
							<td class="center hidden-xs">
								<button class="btn btn-default edit-kelas"><i class="fa fa-pencil-square-o"></i></button>
								<button class="btn btn-default delete-kelas"><i class="fa fa-times"></i></button>
							</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">7</td>
							<td>SECABA PK THP II</td>
							<td class="center hidden-xs">20 Orang</td>
							<td class="center hidden-xs">90 Hari</td>
							<td class="center hidden-xs">Lihat Kalender</td>
							<td class="center hidden-xs">Pusdik Armed</td>
							<td class="center hidden-xs">file</td>
							<td class="center hidden-xs">
								<button class="btn btn-default edit-kelas"><i class="fa fa-pencil-square-o"></i></button>
								<button class="btn btn-default delete-kelas"><i class="fa fa-times"></i></button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="generate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:999999">
	<div class="modal-dialog" style="top:190px;width: 400px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Generate Username & Password</h4>
			</div>							
			<div class="modal-body">
				<form class="form-horizontal" role="form">
				  <div class="form-group">
					<label for="inputEmail3">Username</label>
					<div>
					  <input type="text" class="form-control" value="889756765" disabled>
					</div>
				  </div>
				  <div class="form-group">
					<label for="inputPassword3">Password</label>
					<div>
					  <input type="text" class="form-control" value="12dd343sd" disabled>
					</div>
				  </div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="top: 90px;">
		<div class="modal-content">
			

			<div class="row">
				<div class="col-md-12">
					<div class="box border" style="border-color:#fff;">
						<div class="box-body">
							<div class="tabbable header-tabs">
							 	<ul class="nav nav-tabs">
								  	<li>
								  		<a href="#anggaran" id="link-anggaran" data-toggle="tab"><i class="fa fa-money"></i><span class="hidden-inline-mobile">Anggaran</span></a>
								  	</li>
								  	<li>
								  		<a href="#peserta" id="link-peserta" data-toggle="tab"><i class="fa fa-user"></i><span class="hidden-inline-mobile">Peserta Didik</span></a>
								  	</li>
								  	<li>
								  		<a href="#jadwal" id="link-jadwal" onclick="$('#calendar').fullCalendar('render');" data-toggle="tab"><i class="fa fa-calendar"></i><span class="hidden-inline-mobile">Jadwal</span></a>
								  	</li>
								  	<li>
								  		<a href="#materi" id="link-materi" data-toggle="tab"><i class="fa fa-list-ul"></i><span class="hidden-inline-mobile">Materi</span></a>
								  	</li>
								  	<li class="active">
								  		<a href="#umum" id="link-umum" data-toggle="tab"><i class="fa fa-briefcase"></i><span class="hidden-inline-mobile">Umum</span></a>
								  	</li>
							 	</ul>
							 </div>
							  

							<div class="tab-content">
							  	<div class="tab-pane fade in active" id="umum">
									<form class="form-horizontal" role="form">
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-3 control-label">Pilih Program</label>
											<div class="col-sm-3">
												<select class="form-control">
												  <option>Program Pendidikan</option>
												  <option>Program Kepelatihan</option>
												  <option>Program Keahlian</option>
												  <option>Program Kepemimpinan</option>
												  <option>Program Umum</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Struktur Organisasi Satdik</label>
											<div class="col-sm-3">
											  <input type="email" class="form-control">
											</div>
										</div>
									</form>	
									<hr />
									<button type="button" class="btn btn-success pull-left"><i class="fa fa-check"></i> Simpan</button>
									<button onclick="$('#link-materi').trigger('click');" type="button" class="btn btn-default pull-right">Lanjut <i class="fa fa-arrow-right"></i></button>
							  		<br />
							  	</div>
							  	<div class="tab-pane fade" id="materi">
									<div class="row">
										<div class="col-md-6">
											<button class="btn btn-light-grey"><i class="fa fa-plus"></i> Tambah</button>
										</div>
										<div class="col-md-2">&nbsp;</div>
										<div class="col-md-4">
											<div class="input-group pull-right">
											  <input type="text" class="form-control" placeholder="Cari ...">
											  <span class="input-group-addon"><i class="fa fa-search"></i></span>
											</div>
										</div>
									</div>
									<div class="divide-20"></div>
									<div class="row">
										<div class="col-md-12">
											<table id="list-materi" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th width="20px;" class="center hidden-xs">No.</th>
														<th>Program</th>
														<th class="center hidden-xs">Jml. Peserta</th>
														<th class="center hidden-xs">Durasi</th>
														<th class="center hidden-xs">Waktu Pelaksanaan</th>
														<th class="center hidden-xs">Tempat Pelaksanaan</th>
														<th class="center hidden-xs">Dokumen</th>
														<th class="center hidden-xs" nowrap>Aksi</th>
													</tr>
												</thead>
												<tbody>
													<tr class="gradeX">
														<td class="center hidden-xs">1</td>
														<td>SUSDANYON</td>
														<td class="center hidden-xs">20 Orang</td>
														<td class="center hidden-xs">90 Hari</td>
														<td class="center hidden-xs">Lihat Kalender</td>
														<td class="center hidden-xs">Pusdik Armed</td>
														<td class="center hidden-xs">file</td>
														<td class="center hidden-xs" nowrap>
															<button class="btn btn-default edit-kelas"><i class="fa fa-pencil-square-o"></i></button>
															<button class="btn btn-default delete-kelas"><i class="fa fa-times"></i></button>
														</td>
													</tr>
													<tr class="gradeX">
														<td class="center hidden-xs">2</td>
														<td>SUSDANRAI</td>
														<td class="center hidden-xs">20 Orang</td>
														<td class="center hidden-xs">90 Hari</td>
														<td class="center hidden-xs">Lihat Kalender</td>
														<td class="center hidden-xs">Pusdik Armed</td>
														<td class="center hidden-xs">file</td>
														<td class="center hidden-xs">
															<button class="btn btn-default edit-kelas"><i class="fa fa-pencil-square-o"></i></button>
															<button class="btn btn-default delete-kelas"><i class="fa fa-times"></i></button>
														</td>
													</tr>
													<tr class="gradeX">
														<td class="center hidden-xs">3</td>
														<td>SUSPATIH</td>
														<td class="center hidden-xs">20 Orang</td>
														<td class="center hidden-xs">90 Hari</td>
														<td class="center hidden-xs">Lihat Kalender</td>
														<td class="center hidden-xs">Pusdik Armed</td>
														<td class="center hidden-xs">file</td>
														<td class="center hidden-xs">
															<button class="btn btn-default edit-kelas"><i class="fa fa-pencil-square-o"></i></button>
															<button class="btn btn-default delete-kelas"><i class="fa fa-times"></i></button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<hr />
									<!--<button type="button" class="btn btn-success pull-left"><i class="fa fa-check"></i> Simpan</button>-->
									<button onclick="$('#link-jadwal').trigger('click');" type="button" class="btn btn-default pull-right">Lanjut <i class="fa fa-arrow-right"></i></button>
									<button onclick="$('#link-umum').trigger('click');" type="button" class="btn btn-default pull-right" style="margin-right:5px;"><i class="fa fa-arrow-left"></i> Kembali</button>
							  		<br />
							  	</div>
							  	<div class="tab-pane fade" id="jadwal">
									<div class="row">
										<div class="col-md-3">
											<div class="input-group">
												 <input type="text" value="" class="form-control" placeholder="Event Title..." id="event-title" />
												 <span class="input-group-btn">
													<a href="javascript:;" id="add-event" class="btn btn-success">Add Event</a>
												 </span>
										    </div>
											<div class="divide-20"></div>
											<div id='external-events'>
												<h4>Draggable Events</h4>
												<div id="event-box">
													<div class='external-event'>My Event 1</div>
													<div class='external-event'>My Event 2</div>
													<div class='external-event'>My Event 3</div>
													<div class='external-event'>My Event 4</div>
													<div class='external-event'>My Event 5</div>
												</div>
												<p>
												<input type='checkbox' id='drop-remove' class="uniform"/> <label for='drop-remove'>remove after drop</label>
												</p>
											</div>
										</div>
										<div class="col-md-9">
											<div id='calendar'></div>
										</div>
									</div>
									<hr />
									<!--<button type="button" class="btn btn-success pull-left"><i class="fa fa-check"></i> Simpan</button>-->
									<button onclick="$('#link-peserta').trigger('click');" type="button" class="btn btn-default pull-right">Lanjut <i class="fa fa-arrow-right"></i></button>
									<button onclick="$('#link-materi').trigger('click');" type="button" class="btn btn-default pull-right" style="margin-right:5px;"><i class="fa fa-arrow-left"></i> Kembali</button>
							  		<br />
							  	</div>
							  	<div class="tab-pane fade" id="peserta">
							  		<div class="row">
										<div class="col-md-6">
											<button class="btn btn-light-grey"><i class="fa fa-plus"></i> Tambah</button>
										</div>
										<div class="col-md-2">&nbsp;</div>
										<div class="col-md-4">
											<div class="input-group pull-right">
											  <input type="text" class="form-control" placeholder="Cari ...">
											  <span class="input-group-addon"><i class="fa fa-search"></i></span>
											</div>
										</div>
									</div>
									<div class="divide-20"></div>
									<div class="row">
										<div class="col-md-12">
											<table id="list-peserta" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th width="20px;" class="center hidden-xs">No.</th>
														<th>Nama</th>
														<th class="center hidden-xs">NRP</th>
														<th class="center hidden-xs">Pangkat</th>
														<th class="center hidden-xs">Kecabangan</th>
														<th class="center hidden-xs">Kesatuan</th>
														<th class="center hidden-xs">Jabatan</th>
														<th class="center hidden-xs">Generate</th>
													</tr>
												</thead>
												<tbody>
													<tr class="gradeX">
														<td class="center hidden-xs">1</td>
														<td>Andi Harahap</td>
														<td class="center hidden-xs">678890</td>
														<td class="center hidden-xs">Tamtama</td>
														<td class="center hidden-xs">Secaba</td>
														<td class="center hidden-xs">AD</td>
														<td class="center hidden-xs">Prajurit</td>
														<td class="center hidden-xs"><button class="btn btn-default edit-kelas" data-toggle="modal" data-target="#generate"><i class="fa fa-share-square-o"></i></button></td>
													</tr>
													<tr class="gradeX">
														<td class="center hidden-xs">2</td>
														<td>Rommel Sitompul</td>
														<td class="center hidden-xs">389987</td>
														<td class="center hidden-xs">Tamtama</td>
														<td class="center hidden-xs">Secaba</td>
														<td class="center hidden-xs">AD</td>
														<td class="center hidden-xs">Prajurit</td>
														<td class="center hidden-xs"><button class="btn btn-default edit-kelas" data-toggle="modal" data-target="#generate"><i class="fa fa-share-square-o"></i></button></td>
													</tr>
													<tr class="gradeX">
														<td class="center hidden-xs">3</td>
														<td>Heri Purnama</td>
														<td class="center hidden-xs">238977</td>
														<td class="center hidden-xs">Tamtama</td>
														<td class="center hidden-xs">Secaba</td>
														<td class="center hidden-xs">AD</td>
														<td class="center hidden-xs">Prajurit</td>
														<td class="center hidden-xs"><button class="btn btn-default edit-kelas" data-toggle="modal" data-target="#generate"><i class="fa fa-share-square-o"></i></button></td>
													</tr>
													<tr class="gradeX">
														<td class="center hidden-xs">4</td>
														<td>Basuki Triono</td>
														<td class="center hidden-xs">239089</td>
														<td class="center hidden-xs">Tamtama</td>
														<td class="center hidden-xs">Secaba</td>
														<td class="center hidden-xs">AD</td>
														<td class="center hidden-xs">Prajurit</td>
														<td class="center hidden-xs"><button class="btn btn-default edit-kelas" data-toggle="modal" data-target="#generate"><i class="fa fa-share-square-o"></i></button></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<hr />
									<!--<button type="button" class="btn btn-success pull-left"><i class="fa fa-check"></i> Simpan</button>-->
									<button onclick="$('#link-anggaran').trigger('click');" type="button" class="btn btn-default pull-right">Lanjut <i class="fa fa-arrow-right"></i></button>
									<button onclick="$('#link-jadwal').trigger('click');" type="button" class="btn btn-default pull-right" style="margin-right:5px;"><i class="fa fa-arrow-left"></i> Kembali</button>
							  		<br />
							  	</div>
							  	<div class="tab-pane fade" id="anggaran">
							  		<form class="form-horizontal" role="form">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Jumlah Anggaran</label>
											<div class="col-sm-3">
											  <input type="email" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">Unggah Rincian Anggaran</label>
											<div class="col-sm-5 fileupload-buttonbar">
												<a class="btn btn-light-grey fileinput-button">
													<i class="fa fa-plus"></i>
													<span>Pilih files...</span>
												</a>
											</div>
										</div>
									</form>	
									<hr />
									<button type="button" class="btn btn-success pull-left"><i class="fa fa-check"></i> Simpan</button>
									<button onclick="$('#link-peserta').trigger('click');" type="button" class="btn btn-default pull-right"><i class="fa fa-arrow-left"></i> Kembali</button>
							  		<br />
							  	</div>
							  </div>
						   </div>
						</div>
					</div>
					<!-- /BOX -->
				</div>
			


			
		</div>
	</div>
</div>

<script>

	$('#list-program').dataTable({
		"sDom": "<'clear'>t<'pull-left'i><'pull-right'p>","oLanguage": {	"oPaginate": { "sPrevious": "Prev","sNext": "Next" } },"aoColumns": [null,null,null,null,null,null,null,{ "bSortable": false }	]
	});
	$('#list-materi').dataTable({
		"sDom": "<'clear'>t<'pull-left'p>","oLanguage": {	"oPaginate": { "sPrevious": "Prev","sNext": "Next" } },"aoColumns": [null,null,null,null,null,null,null,{ "bSortable": false }	]
	});
	$('#list-peserta').dataTable({
		"sDom": "<'clear'>t<'pull-left'p>","oLanguage": {	"oPaginate": { "sPrevious": "Prev","sNext": "Next" } },"aoColumns": [null,null,null,null,null,null,null,{ "bSortable": false }	]
	});

$(document).ready(function(){
	
	var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

	var initDrag = function (el) {
		
			// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
			// it doesn't need to have a start or end
			var eventObject = {
				title: $.trim(el.text()) // use the element's text as the event title
			};
			
			// store the Event Object in the DOM element so we can get to it later
			el.data('eventObject', eventObject);
			
			// make the event draggable using jQuery UI
			el.draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});
			
		}
		
		var addEvent = function (title) {
            title = title.length == 0 ? "Untitled Event" : title;
            var html = $('<div class="external-event">' + title + '</div>');
            jQuery('#event-box').append(html);
            initDrag(html);
        }

        $('#external-events div.external-event').each(function () {
            initDrag($(this))
        });

        $('#add-event').unbind('click').click(function () {
            var title = $('#event-title').val();
            addEvent(title);
        });

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        editable: true,
        droppable: true,
        drop: function(date, allDay) { // this function is called when something is dropped
			
				// retrieve the dropped element's stored Event Object
				var originalEventObject = $(this).data('eventObject');
				
				// we need to copy it, so that multiple events don't have a reference to the same object
				var copiedEventObject = $.extend({}, originalEventObject);
				
				// assign it the date that was reported
				copiedEventObject.start = date;
				copiedEventObject.allDay = allDay;
				
				// render the event on the calendar
				// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
				$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
				
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
				
			},
        events: [
            {
                title: 'Matematika',
								start: new Date(y, m, d, 12, 30),
								end: new Date(y, m, d, 14, 0),
								allDay: false,
								backgroundColor: Theme.colors.green,
            },
            {
                title: 'Fisika',
								start: new Date(y, m, d+3, 8, 0),
								end: new Date(y, m, d+3, 10, 0),
								allDay: false,
								backgroundColor: Theme.colors.primary,
            },
            {
                title: 'Kimia',
								start: new Date(y, m, d+5, 15, 0),
								end: new Date(y, m, d+5, 18, 0),
								allDay: false,
								backgroundColor: Theme.colors.grey,
            },
            {
                title: 'UTS Matematika',
								start: new Date(y, m, d+15, 15, 0),
								end: new Date(y, m, d+15, 18, 0),
								allDay: false,
								backgroundColor: Theme.colors.red,
            },
        ]
    });

    });


</script>