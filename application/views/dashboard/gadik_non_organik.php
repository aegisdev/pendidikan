<style type="text/css">
	.box.border.green > .box-title{
		background-color: #8e9e82;
	}
</style>
<div class="box border green">
	<div class="box-title">
		<h4><i class="fa fa-bookmark-o"></i> <span class="hidden-inline-mobile">Gadik Non Organik</span></h4>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				<button class="btn btn-success" id="tambah" data-toggle="modal" data-target="#myModal">
					<i class="fa fa-plus"></i> Tambah Gadik Non Organik
				</button>
			</div>
			<div class="col-md-3">&nbsp;</div>
			<div class="col-md-3">
				<div class="input-group pull-right">
				  <input type="text" class="form-control" placeholder="Cari ...">
				  <span class="input-group-addon"><i class="fa fa-search"></i></span>
				</div>
			</div>
		</div>
		<div class="divide-20"></div>
		<div class="row">
			<div class="col-md-12">
				<table id="list-gadik-non-organik" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="20px;" class="center hidden-xs">No.</th>
							<th>&nbsp;</th>
							<th class="center hidden-xs">Nama</th>
							<th class="center hidden-xs">Pangkat</th>
							<th class="center hidden-xs">Korps</th>
							<th class="center hidden-xs">NRP</th>
							<th class="center hidden-xs">Jabatan</th>
							<th class="center hidden-xs">Keterangan</th>
						</tr>
					</thead>
					<tbody>
						<tr class="gradeX">
							<td class="center hidden-xs">1</td>
							<td class="center hidden-xs" nowrap>
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">2</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">3</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">4</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">5</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">6</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
						<tr class="gradeX">
							<td class="center hidden-xs">7</td>
							<td class="center hidden-xs">
								<button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
							</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
							<td class="center hidden-xs">&nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:999999">
	<div class="modal-dialog" style="width: 600px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Tambah Gadik Non Organik</h4>
			</div>							
			<div class="modal-body" style="max-height:450px;overflow:auto;">
				<form class="form-horizontal" role="form">
				  	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Nama</label>
						<div class="col-sm-8">
					 		<input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">NRP / NIP / NIK</label>
						<div class="col-sm-8">
					 		<input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Jenis Kelamin</label>
						<div class="col-sm-8">
					 		<input type="radio" />Laki - laki
					 		<input type="radio" />Perempuan
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Pangkat</label>
						<div class="col-sm-8">
					 		<input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Korps</label>
						<div class="col-sm-8">
					 		<input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Kesatuan</label>
						<div class="col-sm-8">
					 		<input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Jabatan</label>
						<div class="col-sm-8">
					 	 <input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">T.M.T Jabatan</label>
						<div class="col-sm-8">
					 	 <input type="email" class="form-control">
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Tingkat Jabatan</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Golongan Jabatan</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Tingkat Jabatan</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Tgl Lahir</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Kota Lahir</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Kategori</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">TMT Kategori</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Sumber PA</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Tahun PA</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">T.M.T TNI</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">T.M.T Perwira</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Agama</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Suku Bangsa</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<hr />
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Tahun PA</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">T.M.T TNI</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">T.M.T Perwira</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Agama</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Suku Bangsa</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Kebangsaan</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Status Kawin</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Tinggi Badan</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Berat Badan</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Warna Kulit</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Bentuk Rambut</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Jumlah Anak</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Klasifikasi Psikologis</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				 	<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Kualifikasi Psikologis</label>
						<div class="col-sm-8">
							<textarea class="form-control"></textarea>
						</div>
				 	</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success pull-left"><i class="fa fa-check"></i> Simpan</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>
		</div>
	</div>
</div>

<script>

	$('#list-gadik-non-organik').dataTable({
		"sDom": "<'clear'>t<'pull-left'i><'pull-right'p>","oLanguage": {	"oPaginate": { "sPrevious": "Prev","sNext": "Next" } },"aoColumns": [null,{ "bSortable": false },null,null,null,null,null,null	]
	});


</script>