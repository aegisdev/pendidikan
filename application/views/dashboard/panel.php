<script src="<?php echo base_url();?>assets/panel/js/modernizr.custom.25376.js"></script>
<style>
.component{position:relative;}
.effect-rotatetop.animate .containers 
{
	-webkit-transform: translateZ(-1500px) translateY(65%);
	transform: translateZ(-1500px) translateY(45%); 
}
.outer-nav.horizontal a 
{
    margin-left: 15px;
    margin-right: 15px;
}
  </style>
<div class="row-fluid navigate">
  <div id="perspective" class="span12 perspective effect-rotatetop">
    <div class="containers">
      <div class="wrapper"><!-- wrapper needed for scroll -->
        <div class="main clearfix">
          <div class="column">
            <p><button class="button" id="showMenu">Launch</button></p>
          </div>
        </div><!-- /main -->
      </div><!-- wrapper -->
    </div><!-- /container -->
    <nav class="outer-nav bottom horizontal">
      <a href="<?=base_url();?>home/panel_session/administrasi" class="icons-star">Administrasi Pendidikan</a>
      <a href="<?=base_url();?>home/panel_session/operasional" class="icons-image">Operasional Pendidikan</a>   
    </nav>
  </div><!-- /perspective -->
</div>
<script src="<?php echo base_url();?>assets/panel/js/classie.js"></script>
<script src="<?php echo base_url();?>assets/panel/js/menu.js"></script>
<script>
$(".navigate").bind("mousewheel", function() { return false;});
</script>