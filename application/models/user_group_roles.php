<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User_Group_Roles extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_menu_group_roles($id){
    	$this->db->select('*');
    	$this->db->from('tbl_menu');
    	$this->db->join('tbl_user_group_role', 'tbl_menu.id = tbl_user_group_role.menu_id', 'left');
    	$this->db->where('tbl_user_group_role.user_group_id',$id);
    	$this->db->or_where(array('tbl_user_group_role.user_group_id' => NULL));
    	$q = $this->db->get();

    	return $q->result();
    }

    function find_by_user_group($id){
      $this->db->where(array('user_group_id' => $id));
      $q = $this->db->get('tbl_user_group_role');

      if($q->num_rows() > 0){
        return $q->result_array();
      } else {
        return 0;
      }
    }

    function remove_by_user_group($id){
      $q = $this->db->delete('tbl_user_group_role', array('user_group_id' => $id));

      return $q; 
    }

    function create($data){
      $q = $this->db->insert('tbl_user_group_role', $data);

      return $q;
    }
}