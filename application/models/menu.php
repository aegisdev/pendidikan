<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Menu extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }


    function get_menu($menu_user = null){

    	$this->db->where('parent_id',0);
    	if($menu_user){
			$this->db->where_in('id', $menu_user);    		
    	}
        $this->db->order_by('order_no','ASC');
  		$query = $this->db->get('tbl_menu');  	
  	// 	$result = array();
  	// 	if ($query->num_rows() > 0){
		 //   	foreach ($query->result() as $row){
			// 		if($row->url != "#"){
			// 			array_push($result, $row);
			// 		} else {
			// 			$self->db->where('parent_id', $row->order_no);
			// 			$query_2 = $self->db->get('tbl_menu');
			// 			$result_2 = array();
			// 			$child = array();
			// 			foreach ($query_2->result() as $row_2) {
			// 				array_push($child,$row_2);			      		
			// 			}
			// 			$row->child = $child;
			// 			array_push($result, $row);
			// 		}
		 //   	}
			// }

			// return $result;
    	return $query->result();
    }

    function get_menu_by_group($id){
    	$this->db->select('menu_id');
    	$this->db->where('user_group_id', $id);
    	$q = $this->db->get('tbl_user_group_role');
    	$arr = array();

    	foreach ($q->result_array() as $item) {
    		array_push($arr, $item['menu_id']);
    	}

    	return $arr;
    }

    function get_menu_group_roles($id){
    	$this->db->select('*');
    	$this->db->from('tbl_menu');
    	$this->db->join('tbl_user_group_role', 'tbl_menu.id = tbl_user_group_role.menu_id', 'left');
    	$this->db->where('tbl_user_group_role.user_group_id',$id);
    	$this->db->or_where(array('tbl_user_group_role.user_group_id' => NULL));
    	$q = $this->db->get();

    	return $q->result();
    }

    function find_all($opt= false, $condition = array(), $cols = array()){
      if($opt){
        //selected columns
        if(count($cols) > 0){
          $this->db->select(implode(",", $cols));
        }

        //condition
        if(count($condition) > 0){
          foreach ($condition as $key => $value) {
            $this->db->where($key, $value);
          }
        }
      }

        $q = $this->db->get('tbl_menu');

        if($q->num_rows() > 0){
            return $q->result_array();
        } else {
            return array();
        }
    }
}