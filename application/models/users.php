<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Users extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }


    function find_by_username_password($username, $password){
    	$this->db->where(array('name' => $username, 'password' => md5($password)));
    	$q = $this->db->get('tbl_users');

    	if($q->num_rows() > 0){
    		//return $q->result_array()[0];
            $u = $q->result_array();
            return $u[0];
    	} else {
    		return array();
    	}
    }

    function find_all(){
    	$q = $this->db->get('tbl_users');

    	if($q->num_rows() > 0){
    		return $q->result_array();
    	} else {
    		return 0;
    	}
    }

    function find($id){
    	$this->db->where(array('id' => $id));
    	$q = $this->db->get('tbl_users');

    	if($q->num_rows() > 0){
    		return $q->result_array();
    	} else {
    		return 0;
    	}
    }

    function create($user){
    	$q = $this->db->insert('tbl_users', $user);

    	return $q;
    }

    function update($id, $data){
    	$this->db->where('id', $id);
    	$q = $this->db->update('tbl_users', $data);

    	return $q;
    }

    function remove($id){
  		$q = $this->db->delete('tbl_users', array('id' => $id));

  		return $q; 
    }
}