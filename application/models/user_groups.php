<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User_Groups extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function find_all(){
    	$q = $this->db->get('tbl_user_groups');

    	if($q->num_rows() > 0){
    		return $q->result_array();
    	} else {
    		return 0;
    	}
    }

    function find($id){
      $this->db->where(array('id' => $id));
      $q = $this->db->get('tbl_user_groups');

      if($q->num_rows() > 0){
        return $q->result_array();
      } else {
        return 0;
      }
    }

    function create($user){
      $q = $this->db->insert('tbl_user_groups', $user);

      return $q;
    }

    function update($id, $data){
      $this->db->where('id', $id);
      $q = $this->db->update('tbl_user_groups', $data);

      return $q;
    }

    function remove($id){
      $q = $this->db->delete('tbl_user_groups', array('id' => $id));

      return $q; 
    }
}