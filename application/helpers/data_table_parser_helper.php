<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('dt_parse_data'))
{
  function dt_parse_data($data = array())
  {
  	$temp = array();
  	$temp['aaData'] = array();

  	for ($i=0; $i < count($data); $i++) { 
	  	$rows = array();
	  	foreach ($data[$i] as $key => $value) {
	   		array_push($rows, $value);
	   	}	

	   	$temp['aaData'][$i] = $rows;
  	}

   	return $temp;
  }   
}