<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
		$this->load->view('404.html');	
	}

	public function login(){
		$this->load->view('dashboard/login');
	}

	public function do_login(){
		$this->session->set_userdata(array('logged_in' => TRUE));
		redirect(base_url());
		
		/*
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if($username == 'aegisdev' && $password == 'aegisdev'){
			$user = array(
				'logged_in' => TRUE,
				'id' => 99,
				'nik' => 9999,
				'name' => 'Aegis Dev',
				'user_group_id' => 1,
				'status' => 1
			);

			$this->session->set_userdata($user);
			redirect(base_url());
			exit();
		}


		$user = $this->users->find_by_username_password($username, $password);

		if($user){
			$user['logged_in'] = TRUE;
			$this->session->set_userdata($user);
			$this->db->insert('tbl_global_logs', array('id_event'=> 10, 'id_user'=>$user['id'], 'description'=> 'login', 'severity'=>'normal'));
			redirect(base_url());
		} else {
			$this->session->set_flashdata('message', 'Incorrect username and password!');
			redirect(base_url() . 'auth/login');
		}
		*/
	}

	public function logout(){
		$this->session->sess_destroy();	
		redirect(base_url() . 'auth/login');
	}
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */