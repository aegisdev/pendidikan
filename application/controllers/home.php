<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();			
		if (!$this->session->userdata('logged_in')) { 
			redirect('/auth/login'); 
		} 
	}

	public function index()
	{
		$this->load->view('partial/header');
		$this->load->view('dashboard/content');
		$this->load->view('partial/footer');
	}
	
	public function panel_session($str)
	{
		$this->session->unset_userdata(array('panel_session'=>''));
		$this->session->set_userdata(array('panel_session'=>$str));
		redirect(base_url()); 
	}

	public function panel()
	{
		$this->load->view('dashboard/panel');
	}

	public function dashboard()
	{
		$this->load->view('dashboard/main_dashboard');
	}

	public function kotaksurat()
	{
		$this->load->view('dashboard/kotaksurat');
	}

	public function programpendidikan()
	{
		$this->load->view('dashboard/programpendidikan');
	}

	public function faq()
	{
		$this->load->view('dashboard/faq');
	}

	public function pengguna()
	{
		$this->load->view('dashboard/pengguna');
	}

	public function kurikulum()
	{
		$this->load->view('dashboard/kurikulum');
	}

	public function gadik_organik()
	{
		$this->load->view('dashboard/gadik_organik');
	}

	public function gadik_non_organik()
	{
		$this->load->view('dashboard/gadik_non_organik');
	}

	public function gapendik()
	{
		$this->load->view('dashboard/gapendik');
	}

	public function metode_pengajaran()
	{
		$this->load->view('dashboard/metode_pengajaran');
	}

	public function alins_alongins()
	{
		$this->load->view('dashboard/alins_alongins');
	}

	public function fasilitas_pendidikan()
	{
		$this->load->view('dashboard/fasilitas_pendidikan');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */